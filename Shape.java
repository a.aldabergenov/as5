package assign3;

import java.util.Scanner;

public class Shape{
	protected String color;
	protected boolean filled;
	
	public Shape() {
		this.setColor("green");
		this.setFilled(true);
	}
	
	public Shape(String col, boolean fill) {
		this.setColor(col);
		this.setFilled(fill);
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	public boolean isFilled() {
		return filled;
	}
	
	public String toString() {
		String str = "A Shape with color of " + this.getColor() + " and ";
		if(this.isFilled()) {
			str+="filled.";
		}
		else {
			str+="Not filled.";
		}
		return str;
	}
	
	public static void main(String args[]){
		Scanner in = new Scanner(System.in);
		Shape s = new Shape("red", true);
		if(s.isFilled())
			System.out.println(s.getColor());
		else 
			System.out.println("Not filled");
		
		String col = in.next();
		s.setColor(col);
		if(s.isFilled())
			System.out.println(s.getColor());
		else 
			System.out.println("Not filled");
		
		s.setFilled(false);
		if(s.isFilled())
			System.out.println(s.getColor());
		else 
			System.out.println("Not filled");
		in.close();
	}
	
}